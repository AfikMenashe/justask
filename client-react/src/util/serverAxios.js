import axios from 'axios';

// Set config defaults when creating the instance
const serverAxios = axios.create({
  baseURL: 'http://localhost:8080',
});

serverAxios.interceptors.request.use(
  async (config) => {
    const token = localStorage.getItem('token');

    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
  },
  (err) => {
    return Promise.reject(err);
  }
);

export default serverAxios;
