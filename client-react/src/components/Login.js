import { React, useState, useEffect } from 'react';
import { Modal } from 'react-bootstrap';
import serverAxios from '../util/serverAxios';

const loginFunc = (
  email,
  password,
  onHide,
  setToken,
  setErrorNum,
  setErrorPopup
) => {
  serverAxios
    .post(`/users/login`, {
      email: email,
      password: password,
    })
    .then((res) => {
      setToken(res.data.token);
      localStorage.setItem('userLoggedInID', res.data.user._id);
      if (res.data.user.isAdmin === true) {
        localStorage.setItem('userAdmin', 'true');
      }
      onHide();
    })
    .catch((e) => {
      console.error(e);
      setErrorNum(e.response.status);
      setErrorPopup(true);
    });
};

const registerFunc = (
  name,
  email,
  password,
  onHide,
  setToken,
  setErrorNum,
  setErrorPopup
) => {
  const user = {
    name: name,
    email: email,
    password: password,
    isAdmin: false,
  };

  serverAxios
    .post(`/users/addUser`, { user })
    .then((res) => {
      // after the User successfully created login to website
      loginFunc(email, password, onHide, setToken, setErrorNum, setErrorPopup);
    })
    .catch((e) => {
      console.error(e);
      setErrorNum(e.response.status);
      setErrorPopup(true);
    });
};
function Form({ option, onHide, setToken, setErrorNum, setErrorPopup }) {
  var [Email, setEmail] = useState('');
  var [Password, setPassword] = useState('');
  var [Name, setName] = useState('');

  const submitClicked = (
    option,
    onHide,
    setToken,
    setErrorNum,
    setErrorPopup
  ) => {
    if (option === 1) {
      // option === 1 -> sign-in
      loginFunc(Email, Password, onHide, setToken, setErrorNum, setErrorPopup);
    } else {
      // option === 2 -> sign-up
      registerFunc(
        Name,
        Email,
        Password,
        onHide,
        setToken,
        setErrorNum,
        setErrorPopup
      );
    }
  };
  return (
    <form className="account-form" onSubmit={(evt) => evt.preventDefault()}>
      <div
        className={
          'account-form-fields ' +
          (option === 1 ? 'sign-in' : option === 2 ? 'sign-up' : 'forgot')
        }
      >
        <input
          id="name"
          name="name"
          type={option === 1 ? 'hidden' : 'text'}
          placeholder="Name"
          required
          // hidden={option === 1 ? true : false ? true : false}
          disabled={option === 1 ? true : false}
          onInput={(event) => {
            setName(event.target.value);
          }}
        />
        <input
          id="email"
          name="email"
          type="email"
          placeholder="E-mail"
          required
          onInput={(event) => {
            setEmail(event.target.value);
          }}
        />
        <input
          id="password"
          name="password"
          type="password"
          placeholder="Password"
          required={option === 1 || option === 2 ? true : false}
          onInput={(event) => {
            setPassword(event.target.value);
          }}
        />
        <input
          id="repeat-password"
          name="repeat-password"
          type="password"
          placeholder="Repeat password"
          required={option === 2 ? true : false}
          disabled={option === 1 ? true : false}
        />
      </div>
      <button
        className="btn-submit-form"
        type="submit"
        onClick={() => {
          submitClicked(option, onHide, setToken, setErrorNum, setErrorPopup);
        }}
      >
        {option === 1 ? 'Sign in' : 'Sign up'}
      </button>
    </form>
  );
}

const Login = ({ show, handleClose, setToken, setErrorNum, setErrorPopup }) => {
  const [option, setOption] = useState(1);
  return (
    <Modal show={show} size="lg" centered onHide={handleClose}>
      <Modal.Body>
        <div className="container login-body">
          <header>
            <div
              className={
                'header-headings ' + (option === 1 ? 'sign-in' : 'sign-up')
              }
            >
              <span>Sign in to your account</span>
              <span>Create an account</span>
            </div>
          </header>
          <ul className="options">
            <li
              className={option === 1 ? 'active' : ''}
              onClick={() => setOption(1)}
            >
              Sign in
            </li>
            <li
              className={option === 2 ? 'active' : ''}
              onClick={() => setOption(2)}
            >
              Sign up
            </li>
          </ul>
          <Form
            option={option}
            onHide={handleClose}
            setToken={setToken}
            setErrorNum={setErrorNum}
            setErrorPopup={setErrorPopup}
          />
        </div>
      </Modal.Body>
    </Modal>
  );
};

export default Login;
