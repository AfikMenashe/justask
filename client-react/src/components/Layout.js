import React from "react";
import NavSidebar from "./NavSidebar";
import BodyWrapper from "./BodyWrapper";
import "react-datepicker/dist/react-datepicker.css";
import "../css/FixedSideBarStyle.css";

const Layout = ({ children }) => {
  return (
    <BodyWrapper>
      {/* <div className="flex h-screen bg-gray"> */}
      <div className="sidenav">
        <img
          src={window.location.origin + "/images/justAsk-Logo.png"}
          style={{ marginBottom: "20px" }}
        />
        <NavSidebar />
      </div>

      <div className="main">
        <div className="flex flex-col flex-1 overflow-hidden">
          <main className="content">
            <section className="sm:flex-row flex flex-col flex-1">
              <div
                className="content-box"
                style={{ flexGrow: 2, flexBasis: "0%", marginTop: "90px" }}
              >
                {children}
              </div>
            </section>
          </main>
        </div>
      </div>
      {/* </div> */}
    </BodyWrapper>
  );
};

export default Layout;
