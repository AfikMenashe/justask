import React, { useState } from "react";
import Portal from "@material-ui/core/Portal";
import { makeStyles } from "@material-ui/core/styles";
import TextareaAutosize from "@material-ui/core/TextareaAutosize";
import serverAxios from "../util/serverAxios";
import userToken from "../util/useToken";

const useStyles = makeStyles((theme) => ({
  alert: {
    padding: theme.spacing(0),
    margin: theme.spacing(1, 0),
  },
}));

export default function NewComment(props) {
  const token = localStorage.getItem("token");
  var [commentData, setCommentData] = useState("");
  var [textValid, setTextValid] = useState("");
  const classes = useStyles();
  const [show, setShow] = React.useState(false);
  const container = React.useRef(null);

  const handleClick = () => {
    if (!token) {
      setTextValid("Oops... You Are Not Connected!");
      setShow(false);
    } else {
      setShow(!show);
    }
  };

  const userCommentData = localStorage.getItem("userLoggedInID");

  const sendNewComment = () => {
    serverAxios
      .post(
        `reviews/createReview/`,
        {
          userCommentID: userCommentData,
          CommentFromClientData: commentData,
          PostID: props.postID,
        },
        {
          headers: {
            Authorization: `Bearer  ${localStorage.getItem("token")}`,
          },
        }
      )
      .then((res) => {
        props.renderReviews([...props.allReviewsBefore, res.data]);
        setCommentData("");
      })
      .catch((e) => {
        console.error(e);
      });
  };

  const checkTokenLogin = () => {
    if (!token) setTextValid("Oops... You Are Not Connected!");
    else sendNewComment();
  };

  return (
    <div className="container-fluid">
      <div>
        <button type="button" className="btn btn-primary" onClick={handleClick}>
          {show ? "Close Comment" : "New Comment"}
        </button>
        <div className={classes.alert}>
          {show ? (
            <Portal container={container.current}>
              <TextareaAutosize
                value={commentData}
                style={{ width: "100%", height: "150px" }}
                aria-label="empty textarea"
                placeholder="Empty"
                onChange={(data) => {
                  setCommentData(data.target.value);
                }}
              />
              <button
                type="button"
                className="btn btn-primary"
                onClick={checkTokenLogin}
              >
                <i className="ion ion-md-create" />
                &nbsp; Reply
              </button>
            </Portal>
          ) : null}
        </div>
        <p>{textValid}</p>
        <div className={classes.alert} ref={container} />
      </div>
    </div>
  );
}
