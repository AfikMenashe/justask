import React, { useState, useEffect } from 'react';
import Pagination from './Pagination';
import Review from './Review';
import SpinnerComponent from './SpinnerComponent';
import serverAxios from '../util/serverAxios';
import NewComment from './NewComment';
import SearchReviewBar from './SearchReviewBar';
function Comments(props) {
  var [reviewsList, setReviewsList] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const reviewsPerPage = 6;
  useEffect(() => {
    serverAxios
      .get(`/reviews/getPostReviews/${props.postID}`)
      .then((res) => {
        const reviews = res.data;
        setReviewsList(reviews);
      })
      .catch((e) => {
        props.errorCallback(e);
      });
  }, []);

  const deleteCallbacks = (reviewID) => {
    setReviewsList(reviewsList.filter((review) => review._id != reviewID));
  };

  const updateCallbacks = (reviewID, reviewBody) => {
    setReviewsList(
      reviewsList.map((review) => {
        if (review._id === reviewID) review.body = reviewBody;
        return review;
      })
    );
  };
  //Get current reviews
  const indexOfLastReview = currentPage * reviewsPerPage;
  const indexOfFirstReview = indexOfLastReview - reviewsPerPage;
  const currentReviews = reviewsList.slice(
    indexOfFirstReview,
    indexOfLastReview
  );

  //Paginates -> Change Page
  const paginates = (pageNumber) => setCurrentPage(pageNumber);

  return (
    <>
      <div className="container-fluid">
        <div className="card mb-3">
          <div className="card-header pl-0 pr-0">
            <div className="row no-gutters w-100 align-items-center">
              <div className="col ml-3">Comment</div>
              <div className="col col-11 ">
                {' '}
                <SearchReviewBar
                  setList={setReviewsList}
                  postID={props.postID}
                  errorCallback={props.errorCallback}
                />
              </div>
            </div>
          </div>

          {reviewsList != null ? (
            <div>
              {currentReviews.map((review) => (
                <div className="card mb-3">
                  <Review
                    postID={props.postID}
                    reviewBody={review.body}
                    reviewID={review._id}
                    userObjectAuthor={review.author}
                    date={review.published}
                    price={review.price}
                    content={review.body}
                    deleteCallbacks={deleteCallbacks}
                    updateCallbacks={updateCallbacks}
                  />
                </div>
              ))}
            </div>
          ) : (
            <SpinnerComponent />
          )}

          <hr className="m-0" />
        </div>
        <div>
          <Pagination
            postsPerPage={reviewsPerPage}
            totalPosts={reviewsList.length}
            paginate={paginates}
          />
        </div>

        <div style={{ marginTop: '50px' }}>
          <NewComment
            postID={props.postID}
            renderReviews={setReviewsList}
            allReviewsBefore={reviewsList}
          />
        </div>
      </div>
    </>
  );
}

export default Comments;
