/* eslint-disable react/display-name, jsx-a11y/click-events-have-key-events */
import { Navigation } from 'react-minimal-side-navigation';
import { useHistory, useLocation } from 'react-router-dom';
import Icon from 'awesome-react-icons';
import { React, useEffect, useState, Fragment } from 'react';
import 'react-minimal-side-navigation/lib/ReactMinimalSideNavigation.css';
import Login from './Login';
import userToken from '../util/useToken';
import serverAxios from '../util/serverAxios';
import Error from './Error';

// TODO: UI problem when clicked doesn't re-render

const NavSidebar = () => {
  const { token, setToken } = userToken();
  var [addModalLogin, setAddModalLogin] = useState(false);
  var [top5Tags, setTop5Tags] = useState([]);
  const history = useHistory();
  const location = useLocation();
  // for error pop up
  var [errorPopup, setErrorPopup] = useState(false);
  var [errorNum, setErrorNum] = useState('500');
  useEffect(() => {
    serverAxios
      .get('/tags/top5tags')
      .then((res) => {
        var tags = [];
        for (var i = 0; i < res.data.length; i++) {
          // push the tags to array for presenting in navbar
          tags = [
            ...tags,
            {
              title: `#${res.data[i].type}`,
              itemId: `/tags/${res.data[i]._id}`,
            },
          ];
        }
        setTop5Tags(tags);
      })
      .catch((e) => {
        console.error('Error with top 5 tags: ' + e);
        setErrorNum(e.response.status);
        setErrorPopup(true);
      });
  }, []);

  //default login btn -> before sign in
  var LoginBtn = [
    {
      title: 'Login',
      itemId: 'login',
      elemBefore: () => <Icon name="log-in" />,
    },
  ];
  // if use already signe in -> token is saved in localStorage
  if (token) {
    var userId = localStorage.getItem('userLoggedInID');
    LoginBtn = [
      {
        title: 'Logout',
        itemId: 'logout',
        elemBefore: () => <Icon name="log-out" />,
      },
      {
        title: 'Profile',
        itemId: `/profile/${userId}`,
        elemBefore: () => <Icon name="user" />,
      },
    ];
  }

  if (!top5Tags) return <div></div>;
  return (
    <Fragment>
      <Login
        show={addModalLogin}
        handleClose={() => {
          setAddModalLogin(false);
          history.replace('/', '/');
        }}
        setToken={setToken}
        setErrorNum={setErrorNum}
        setErrorPopup={setErrorPopup}
      />
      <Error
        show={errorPopup}
        handleClose={() => {
          setErrorPopup(false);
        }}
        errorNum={errorNum}
      />
      {/* Sidebar */}
      <div>
        <Navigation
          activeItemId={location.pathname}
          onSelect={({ itemId }) => {
            switch (itemId) {
              case 'login':
                setAddModalLogin(true);
                return;
              case 'logout':
                setToken('');
                history.replace('/', '/');
                // localStorage.removeItem("token");
                localStorage.removeItem('userLoggedInID');
                localStorage.removeItem('userAdmin');
                return;
              default:
                history.push(itemId);
                return;
            }
          }}
          items={[
            {
              title: 'Home',
              itemId: '/',
              elemBefore: () => <Icon name="activity" />,
            },
            ...LoginBtn,
            ,
            {
              title: 'Tags',
              itemId: '/another',
              elemBefore: () => <Icon name="tag" />,
              subNav: [...top5Tags],
            },
          ]}
        />
      </div>

      {localStorage.getItem('userAdmin') === 'true' ? (
        <div className="absolute bottom-0 w-full my-8">
          <Navigation
            activeItemId={location.pathname}
            items={[
              {
                title: 'Scrape Tags',
                itemId: 'scrape',
                elemBefore: () => <Icon name="plus" />,
              },
              {
                title: 'Fetch Users',
                itemId: 'fetchUsers',
                elemBefore: () => <Icon name="plus" />,
              },
              {
                title: 'Fetch Post',
                itemId: 'fetchPost',
                elemBefore: () => <Icon name="plus" />,
              },
            ]}
            onSelect={({ itemId }) => {
              switch (itemId) {
                case 'scrape':
                  serverAxios
                    .get(`/tags/scraper`)
                    .then((res) => {
                      console.log('Scrape tags successed');
                    })
                    .catch((e) => {
                      console.error(e);
                    });
                  return;
                case 'fetchUsers':
                  serverAxios
                    .get(`/api/fetchUsers`)
                    .then((res) => {
                      console.log('Fetch Data successed');
                    })
                    .catch((e) => {
                      console.error(e);
                    });
                  return;
                case 'fetchPost':
                  serverAxios
                    .get(`/api/fetchPosts`)
                    .then((res) => {
                      console.log('Fetch Data successed');
                    })
                    .catch((e) => {
                      console.error(e);
                    });
                  return;
              }
            }}
          />
        </div>
      ) : null}
    </Fragment>
  );
};

export default NavSidebar;
