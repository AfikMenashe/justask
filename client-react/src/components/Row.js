import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { timeAgo } from "../helpers/helpersFuncs";
import serverAxios from "../util/serverAxios";
import SpinnerComponent from "./SpinnerComponent";

function Row(props) {
  const authorOfPost = props.userObjectAuthor;
  var [updatedUser, setUpdatedUser] = useState(null);
  var [MapReduceReviews, setArrMapReduceReviews] = useState(null);

  useEffect(() => {
    serverAxios
      .get(`/reviews/getUpdatedUser/${props.postID}`)
      .then((res) => {
        const updateUser = res.data;
        setUpdatedUser(updateUser);
      })
      .catch((e) => {
        console.error(
          "ERROR with getting user Updated review fetching data: " + e
        );
      });

    /////////////////////////////////////////////////////////////////////////////////////////////
    //----- MapReduce Function -> Showing the numbers of comments for each post ---------//

    // serverAxios
    //   .get(`/posts/mapReduceFunc/${props.postID}`)
    //   .then((res) => {
    //     const ArrObjectMapReduce = res.data;
    //     setArrMapReduceReviews(ArrObjectMapReduce);
    //   })
    //   .catch((e) => {
    //     console.error(
    //       "ERROR with getting Map Reduce Numbers of Review's fetching data: " +
    //         e
    //     );
    //   });
    ///////////////////////////////////////////////////////////////////////////////////////////////
  }, []);

  if (updatedUser == null) {
    return (
      <div>
        <SpinnerComponent />
      </div>
    );
  }

  return (
    <div className="card-body py-3">
      <div className="row no-gutters align-items-center">
        <div className="col">
          {" "}
          <Link to={`/posts/${props.postID}`}>
            <b>
              <p className="text-big" data-abc="true">
                {props.headLine}
              </p>
            </b>
          </Link>
          <Link to={`/posts/${props.postID}`}>
            <div className="text-muted small mt-1">
              {timeAgo(props.date)}{" "}
              <b
                style={{
                  fontSize: "10px",
                }}
              >
                {" "}
                ⚪ by{" "}
              </b>
              <span className="text-muted" data-abc="true">
                {authorOfPost.name}
              </span>
            </div>
          </Link>
        </div>
        <div className="d-none d-md-block col-4">
          <div className="row no-gutters align-items-center">
            <div className="col-4">
              {MapReduceReviews != null ? (
                <div>#{MapReduceReviews.value}</div>
              ) : (
                <div></div>
              )}
            </div>

            {updatedUser != null ? (
              updatedUser.author != null ? (
                <Link to={`/profile/${updatedUser.author._id}`}>
                  <div className="media col-12 align-items-center">
                    {" "}
                    <img
                      src={
                        updatedUser != null
                          ? updatedUser.author != null
                            ? updatedUser.author.image
                            : null
                          : null
                      }
                      className="d-block ui-w-30 rounded-circle"
                    />
                    <div className="media-body flex-truncate ml-2">
                      <div className="line-height-1 text-truncate">
                        {updatedUser != null ? (
                          updatedUser.author != null ? (
                            <div>{updatedUser.author.name}</div>
                          ) : (
                            <div className="text-muted small text-truncate">
                              {" "}
                              Still No Comments
                            </div>
                          )
                        ) : null}
                      </div>{" "}
                      {updatedUser != null ? (
                        <p
                          className="text-muted small text-truncate"
                          data-abc="true"
                        >
                          <span>by {timeAgo(updatedUser.published)}</span>
                        </p>
                      ) : (
                        <div></div>
                      )}
                    </div>
                  </div>
                </Link>
              ) : (
                <div
                  className="details"
                  style={{ fontSize: "12px", fontWeight: "1" }}
                >
                  -- No Comments --
                </div>
              )
            ) : null}
          </div>
        </div>
      </div>
    </div>
  );
}
export default Row;
