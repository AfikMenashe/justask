import { useState, useEffect } from 'react';
import { Col, Row, Form, Button, Container } from 'react-bootstrap';
import Layout from '../Layout';
import serverAxios from '../../util/serverAxios';
import { timeAgo } from '../../helpers/helpersFuncs';
import Error from '../Error';
import { useHistory } from 'react-router-dom';
import Autocomplete from '@material-ui/lab/Autocomplete';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';

// TODO: fix the all tags array

function NewPost() {
  // Hook for redirect page
  let history = useHistory();
  // for error pop up
  var [errorPopup, setErrorPopup] = useState(false);
  var [errorNum, setErrorNum] = useState('500');
  // try to get the token from localStorage
  var token = localStorage.getItem('token');
  const [title, setTitle] = useState('');
  const [body, setBody] = useState('');
  const [selectedImages, setSelectedImages] = useState(null);
  const [userLoggedIn, setUserLoggedIn] = useState([]);

  // <--------------------------- Tags Picker - Autocomplete --------------------------->
  var [tagListForSearch, setTagListForSearch] = useState([]);
  var [loadingTags, setLoadingTags] = useState(false); // determint if the array of tags returned from the server
  var [tagList, setTagList] = useState([]); // array of all tags Id and Name only

  // Filter by tag styles
  const useStyles = makeStyles((theme) => ({
    root: {
      width: 300,
      '& > * + *': {
        marginTop: theme.spacing(3),
      },
    },
  }));
  const classes = useStyles();

  const handleSubmit = (event) => {
    const formData = new FormData();
    formData.append('title', title);
    formData.append('description', body);
    formData.append('author', localStorage.getItem('userLoggedInID'));
    if (selectedImages)
      // check if images selected
      for (let image of selectedImages) {
        formData.append('images', image);
      }
    // get from selected tags only the names of tags
    let slectedTags = tagListForSearch.map(function (item) {
      return item.type;
    });

    formData.append('tags', slectedTags);
    // send post request with all images and data
    serverAxios
      .post('/posts/addPost', formData)
      .then((res) => {
        history.push(`/posts/${res.data._id}`);
      })
      .catch((e) => {
        console.error(e);
        setErrorNum(e.response.status);
        setErrorPopup(true);
      });
  };

  useEffect(() => {
    serverAxios
      .get(`/users/${localStorage.getItem('userLoggedInID')}`)
      .then((res) => {
        const user = res.data;
        setUserLoggedIn(user);
      })
      .catch((e) => {
        console.error(e);
        setErrorNum(e.response.status);
        setErrorPopup(true);
      });

    setLoadingTags(true);
    serverAxios
      .get(`/tags/nameAndId`)
      .then((res) => {
        setTagList(res.data);
        setLoadingTags(false);
      })
      .catch((e) => {
        console.error(e);
        setErrorNum(e.response.status);
        setErrorPopup(true);
      });
  }, []);

  // will hold all the tagss exist in datanase
  const allTags = ['HTML', 'ReactJS', 'NodeJs'];

  if (!token)
    return (
      <Error
        show={true}
        handleClose={() => {
          setErrorPopup(true);
        }}
        errorNum={401}
      />
    );

  return (
    <Layout>
      <Error
        show={errorPopup}
        handleClose={() => {
          setErrorPopup(false);
        }}
        errorNum={errorNum}
      />
      <Form onSubmit={handleSubmit}>
        {/* <Form> */}
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-12">
              <div className="card mb-4">
                <div className="card-header">
                  <div className="media flex-wrap w-100 align-items-center">
                    {' '}
                    <img
                      src={
                        userLoggedIn != null
                          ? userLoggedIn.image != null
                            ? userLoggedIn.image
                            : 'https://lh3.googleusercontent.com/proxy/O8t5v_QWbPEEhebJCW2wReuPSb7-dOIo4-2gR6--Sa4YuzOvQdKzmfnbn5r48jJHhxWVJfPm6rPdoOLUjJV8YPrXwxa9xB8OgN-plko7AySBRt_2PbpkBkk'
                          : null
                      }
                      className="d-block ui-w-30 rounded-circle"
                    />{' '}
                    <div className="media-body ml-3">
                      <Link to={`/profile/${userLoggedIn._id}`}>
                        <a data-abc="true">
                          {userLoggedIn != null ? userLoggedIn.name : null}
                        </a>
                      </Link>
                      <div className="text-muted small">New Post</div>
                    </div>
                    <div className="text-muted small ml-3">
                      <div>
                        Member since{' '}
                        <strong>
                          {userLoggedIn != null
                            ? timeAgo(userLoggedIn.singUpTime)
                            : null}
                        </strong>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="card-body">
                  <Form.Group>
                    <Form.Label>Title</Form.Label>
                    <Form.Control
                      type="text"
                      value={title}
                      onChange={(e) => setTitle(e.target.value)}
                    />
                  </Form.Group>
                  <Form.Group>
                    <Form.Label>Body</Form.Label>
                    <Form.Control
                      as="textarea"
                      rows={3}
                      value={body}
                      onChange={(e) => setBody(e.target.value)}
                    />
                  </Form.Group>
                  <Form.Group>
                    <Autocomplete
                      onChange={(event, value) => {
                        setTagListForSearch(value); // save the selected tags in array
                      }}
                      loading={loadingTags} // until gets the tag list set the component on loading state
                      className={classes.root}
                      multiple
                      id="tags-outlined"
                      options={tagList}
                      getOptionLabel={(option) => option.type}
                      filterSelectedOptions
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          variant="outlined"
                          label="Choose Tags"
                        />
                      )}
                    />
                  </Form.Group>
                </div>
                <div className="card-footer d-flex  px-0 pt-0 pb-3">
                  <Container fluid className="px-4 pt-3">
                    <Row>
                      <Col sm={9}>
                        <Form.File
                          label="Example images input"
                          type="file"
                          name="images"
                          id="exampleFile"
                          onChange={(e) => setSelectedImages(e.target.files)}
                          multiple
                        />
                      </Col>

                      <Col>
                        <Button onClick={handleSubmit}>Submit</Button>
                      </Col>
                    </Row>
                  </Container>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Form>
    </Layout>
  );
}

export default NewPost;
