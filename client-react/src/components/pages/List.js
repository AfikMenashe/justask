import React, { useState, useEffect } from 'react';
import Row from '../Row';
import Layout from '../Layout';
import Pagination from '../Pagination';
import SpinnerComponent from '../SpinnerComponent';
import serverAxios from '../../util/serverAxios';
import SearchPostBar from '../SearchPostBar';
import { Link } from 'react-router-dom';
import Error from '../Error';

function List() {
  // for error pop up
  var [errorPopup, setErrorPopup] = useState(false);
  var [errorNum, setErrorNum] = useState('500');
  // try to get the token from localStorage
  var token = localStorage.getItem('token');

  var [postsList, setPostList] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const postsPerPage = 30;
  var tagId = null;

  //Get current posts
  const indexOfLastPost = currentPage * postsPerPage;
  const indexOfFirstPost = indexOfLastPost - postsPerPage;
  const currentPosts = postsList.slice(indexOfFirstPost, indexOfLastPost);

  const errorCallback = (e) => {
    console.error(e);
    setErrorNum(e.response.status);
    setErrorPopup(true);
  };

  //Paginates -> Change Page
  const paginates = (pageNumber) => setCurrentPage(pageNumber);

  if (window.location.toString().includes('tags')) {
    // if the currect page if related to tags
    const index = window.location.toString().lastIndexOf('/') + 1; // get index of last /
    const id = window.location.toString().substring(index); // get the id after the last /
    tagId = id;
  }
  useEffect(() => {
    if (!tagId) {
      // if tag id not found
      serverAxios
        .get(`/posts`)
        .then((res) => {
          const posts = res.data;
          setPostList(posts);
        })
        .catch((e) => {
          console.error(e);
          setErrorNum(e.response.status);
          setErrorPopup(true);
        });
    } else {
      serverAxios
        .get(`/tags/getposts`, {
          params: {
            tagId: tagId,
          },
        })
        .then((res) => {
          const posts = res.data;
          setPostList(posts);
        })
        .catch((e) => {
          console.error(e);
          setPostList([]);
          setErrorNum(e.response.status);
          setErrorPopup(true);
        });
    }
    token = localStorage.getItem('token');
  }, [tagId]);

  return (
    <Layout>
      <Error
        show={errorPopup}
        handleClose={() => {
          setErrorPopup(false);
        }}
        errorNum={errorNum}
      />
      <div className="d-flex flex-wrap justify-content-between mt-100">
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'flex-start',
            justifyContent: 'space-between',
          }}
        >
          <div>
            {' '}
            {token ? (
              <Link to="/newpost">
                <button
                  type="button"
                  className="btn btn-shadow btn-wide btn-primary"
                >
                  {' '}
                  <span className="btn-icon-wrapper pr-2 opacity-7">
                    {' '}
                    <i className="fa fa-plus fa-w-20" />{' '}
                  </span>{' '}
                  New post{' '}
                </button>{' '}
              </Link>
            ) : null}
          </div>{' '}
          <SearchPostBar setList={setPostList} errorCallback={errorCallback} />
        </div>
      </div>
      <div className="container-fluid">
        <div className="card mb-3">
          <div className="card-header pl-0 pr-0">
            <div className="row no-gutters w-100 align-items-center">
              <div className="col ml-3">Topics</div>
              <div className="col-4 text-muted">
                <div className="row no-gutters align-items-center">
                  <div className="col-4">Comments</div>
                  <div className="col-8">Last update</div>
                </div>
              </div>
            </div>
          </div>

          {postsList != null ? (
            <div>
              {currentPosts.map((post, i) => (
                <Row
                  key={i}
                  userObjectAuthor={post.author}
                  headLine={post.title}
                  date={post.postedTime}
                  price={post.price}
                  postID={post._id}
                />
              ))}
            </div>
          ) : (
            <SpinnerComponent />
          )}

          <hr className="m-0" />
        </div>
        <Pagination
          postsPerPage={postsPerPage}
          totalPosts={postsList.length}
          paginate={paginates}
        />
      </div>
    </Layout>
  );
}

export default List;
