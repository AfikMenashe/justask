import React from 'react';
import '../css/TagStyle.css';

const TagComponent = ({ tagDetails }) => {
  return (
    <div>
      <p className="tag">#{tagDetails.type}</p>
    </div>
  );
};

export default TagComponent;
