import React, { useState, useEffect } from 'react';
import Portal from '@material-ui/core/Portal';
import { makeStyles } from '@material-ui/core/styles';
import { Button, ButtonToolbar } from 'react-bootstrap';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import '../css/FixedSideBarStyle.css';
import serverAxios from '../util/serverAxios';
import Icon from 'awesome-react-icons';

const useStyles2 = makeStyles((theme) => ({
  alert: {
    padding: theme.spacing(0),
    margin: theme.spacing(1, 0),
  },
}));

const SearchPostBar = ({ setList, errorCallback }) => {
  // Filter by tag styles
  const useStyles = makeStyles((theme) => ({
    root: {
      width: 300,
      '& > * + *': {
        marginTop: theme.spacing(3),
      },
    },
  }));
  const classes = useStyles();

  const classes2 = useStyles2();
  const [show, setShow] = React.useState(false);
  const container = React.useRef(null);

  const handleClickFilter = () => {
    setShow(!show);
  };
  var [tagListForSearch, setTagListForSearch] = useState([]);
  var [loadingTags, setLoadingTags] = useState(false); // determint if the array of tags returned from the server
  var [tagList, setTagList] = useState([]); // array of all tags Id and Name only
  const [fromDate, setFromDate] = useState(null);
  const [toDate, setToDate] = useState(null);
  var [searchTitle, setSearchTitle] = useState('');
  // retuen list of all tags with only name and id
  useEffect(() => {
    setLoadingTags(true);
    serverAxios
      .get(`/tags/nameAndId`)
      .then((res) => {
        setTagList(res.data);
        setLoadingTags(false);
      })
      .catch((e) => {
        errorCallback(e);
      });
  }, []);
  function ClearParams() {
    // search for the clera button and click on it
    var clearBtn = document.getElementsByClassName(
      'MuiButtonBase-root MuiIconButton-root MuiAutocomplete-clearIndicator MuiAutocomplete-clearIndicatorDirty'
    )[0];
    if (clearBtn) clearBtn.click();
    setTagListForSearch([]);
    setFromDate(null);
    setToDate(null);
    setSearchTitle('');
    serverAxios
      .get(`/posts`)
      .then((res) => {
        const posts = res.data;
        setList(posts);
      })
      .catch((e) => {
        console.error('ERROR: ' + e);
      });
  }
  function FilterBy3Parameters() {
    // return new arrat with only the tags id
    let slectedTags = tagListForSearch.map(function (item) {
      return item._id;
    });
    serverAxios
      .get(`/posts/search3params`, {
        params: {
          tags: slectedTags,
          titlePost: searchTitle,
          fromDate: fromDate,
          toDate: toDate,
        },
      })
      .then((res) => {
        // change the list to only with the new posts
        setList(res.data);
      })
      .catch((e) => {
        console.error('Error with the filter : ' + e);
      });
  }

  return (
    <>
      <div
        style={{
          marginLeft: '15px',
          justifyItems: 'end',
        }}
      >
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyItems: 'flex-start',
          }}
        >
          <input
            onInput={(event) => {
              setSearchTitle(event.target.value);
            }}
            value={searchTitle}
            type="text"
            className="form-control"
            placeholder="Enter Post Name"
            style={{ width: '300px' }}
          />
          <div style={{ marginLeft: '10px' }}>
            <ButtonToolbar>
              <Button variant="primary" onClick={FilterBy3Parameters}>
                Search
              </Button>
            </ButtonToolbar>
          </div>
          <div style={{ marginLeft: '10px' }}>
            <button
              type="button"
              className="btn btn-primary"
              onClick={handleClickFilter}
            >
              {show ? (
                <div
                  style={{
                    display: 'flex',
                    flexDirection: 'row',
                    justifyItems: 'flex-start',
                    justifyContent: 'space-between',
                  }}
                >
                  <Icon name="settings" />
                  <p style={{ marginLeft: '5px' }}>Close Filter</p>
                </div>
              ) : (
                <div
                  style={{
                    display: 'flex',
                    flexDirection: 'row',
                    justifyItems: 'flex-start',
                    justifyContent: 'space-between',
                  }}
                >
                  <Icon name="settings" />
                  <p style={{ marginLeft: '5px' }}>Filter</p>
                </div>
              )}
            </button>
          </div>
        </div>{' '}
        <div className={classes2.alert}>
          {show ? (
            <Portal container={container.current}>
              <div
                style={{
                  display: 'flex',
                  flexDirection: 'row',
                  justifyItems: 'flex-start',
                  justifyContent: 'space-between',
                }}
              >
                <Autocomplete
                  onChange={(event, value) => {
                    setTagListForSearch(value); // save the selected tags in array
                  }}
                  loading={loadingTags} // until gets the tag list set the component on loading state
                  className={classes.root}
                  multiple
                  id="tags-outlined"
                  options={tagList}
                  getOptionLabel={(option) => option.type}
                  filterSelectedOptions
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      variant="outlined"
                      label="Filter By Tag"
                    />
                  )}
                />
                <DatePicker
                  placeholderText="From Date"
                  className="form-control"
                  selected={fromDate}
                  onChange={(date) => {
                    setFromDate(date);
                  }}
                />
                <DatePicker
                  placeholderText="To Date"
                  className="form-control"
                  selected={toDate}
                  onChange={(date) => {
                    setToDate(date);
                  }}
                />

                <ButtonToolbar>
                  <Button variant="secondary" onClick={ClearParams}>
                    Clear
                  </Button>{' '}
                </ButtonToolbar>
              </div>
            </Portal>
          ) : null}
        </div>
        <div className={classes2.alert} ref={container} />
      </div>
    </>
  );
};

export default SearchPostBar;
