import React from "react";
import "../css/spinner.css";

const SpinnerComponent = () => {
  return (
    <div className="spinner">
      <div className="bounce1"></div>
      <div className="bounce2"></div>
      <div className="bounce3"></div>
    </div>
  );
};

export default SpinnerComponent;
