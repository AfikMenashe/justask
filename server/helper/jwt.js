// const expressJwt = require('express-jwt');
const jwt = require("jsonwebtoken");
// function authJwt() {
//   const secret = process.env.secret;
//   const api = 'localhost:8080/';
//   return expressJwt({
//     secret,
//     algorithms: ['HS256'],
//     // isRevoked: isRevoked
//   }).unless({
//     path: [
//       // {url: /\/public\/uploads(.*)/ , methods: ['GET', 'OPTIONS'] },//Allow client to get photo without being authrized
//       // {url: /\/Posts(.*)/ , methods: ['GET', 'OPTIONS'] },//Allow client to get Post or a list of Posts without being authrized
//       // {url: /\/api\/users\/login/ , methods: ['POST', 'OPTIONS'] }, //TODO: CHECK
//       // {url: /\/api\/users\/addUser/ , methods: ['POST', 'OPTIONS'] }, //TODO: CHECK
//       // `${api}/users/login`, //TODO: CHECK
//       // `${api}/users/addUser`, //TODO: CHECK
//       'localhost:8080/Posts/',
//       '/Posts',
//     ],
//   });
// }

// //check if the token is of admin user
// async function isRevoked(req, payload, done) {
//   if (!payload.isAdmin) {
//     done(null, true);
//   }
//   done();
// }

const verifyToken = (req, res, next) => {
  if (!req.headers.authorization) {
    return res.status(401).send("Unauthorized");
  }
  const token = req.headers.authorization.split(" ")[1];
  if (!token) {
    return res.status(401).send("Unauthorized");
  }
  const payload = jwt.verify(token, process.env.secret);
  if (!payload) {
    return res.status(401).send("Unauthorized");
  }
  req.user = payload;
  next();
};
module.exports = verifyToken;
