const mongoose = require('mongoose');
const { Tag } = require('../models/tag');

module.exports = async function checkIfTagExist(tagName) {
  let tag = await Tag.findOne({ type: tagName });
  // if tag is not found retunr undefined else return the tag object
  if (!tag) return undefined;
  return tag;
};
