let axios = require('axios');
let cheerio = require('cheerio');
const { Tag } = require('../models/tag');
const checkIfTagExist = require('./checkIfTagExist');

const scrape = async (pagesNum) => {
  // get the data from page
  if (pagesNum <= 0 || !pagesNum) {
    throw new Error('pagesNum must be greater than zero');
  }
  for (let i = 1; i <= pagesNum; i++) {
    const page = await axios.get(
      `https://stackoverflow.com/tags?page=${pagesNum}&tab=popular`
    );
    // arrange it in $ just like jquery
    const $ = cheerio.load(page.data);
    // get each card in page
    $('div.grid--cell').each(async function () {
      // get the anchor element witht he tag name
      $('a.post-tag', this).each(async function () {
        const row = $(this);
        const tagName = row.text(); // get the text
        let existTag = await checkIfTagExist(tagName);
        if (!existTag) {
          // if tag doesn't exist create it'
          const createdTag = new Tag({
            type: tagName,
            posts: [],
          });
          await createdTag.save();
          //   console.log('createdTag: ' + tagName);
        }
      });
    });
    console.log(`Page number ${i} Scraped !`);
  }
};

module.exports = scrape;
