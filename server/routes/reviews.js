const express = require("express");
const router = express.Router();
const { Review } = require("../models/review");
const { User } = require("../models/user");
const { Post } = require("../models/Post");

const mongoose = require("mongoose");
const verifyToken = require("../helper/jwt");

//Get all reviews
router.get("/", async (req, res) => {
  const reviewList = await Review.find();

  if (!reviewList) {
    res.status(500).json({ success: false });
  }
  res.send(reviewList);
});

//New Comment For specific User
router.post("/createReview", verifyToken, async (req, res) => {
  var commentUserID = req.body.userCommentID;
  var commentData = req.body.CommentFromClientData;
  var postID = req.body.PostID;

  //validate all required data is passed
  if (!commentUserID || !commentData || !postID) {
    return res
      .status(404)
      .send("The review can not be created - missing parameters");
  }

  //create the review
  const review = new Review({
    author: mongoose.Types.ObjectId(commentUserID),
    published: Date.now(),
    body: commentData,
    post: mongoose.Types.ObjectId(postID),
  });

  // fins the user
  var user = await User.findOne({
    _id: mongoose.Types.ObjectId(commentUserID),
  });
  // insert for user the id of review
  user.created_reviews.push(mongoose.Types.ObjectId(review._id));

  // fins the Post
  var post = await Post.findOne({
    _id: mongoose.Types.ObjectId(postID),
  });
  // insert for user the id of review
  post.reviews.push(mongoose.Types.ObjectId(review._id));

  //save the review to DB
  post.save();
  user.save();
  review.save();
  review.author = user;
  if (!review)
    return res.status(404).send("The review can not be created - DB error");
  return res.status(200).send(review);
});

//Get the last review of an specific post
router.get("/getUpdatedUser/:id", async (req, res) => {
  if (!mongoose.isValidObjectId(req.params.id)) {
    return res.status(400).json({
      success: false,
      message: "Exeption occured.\nPlease try again later",
    });
  }

  const reviewsList = await Review.find({
    post: mongoose.Types.ObjectId(req.params.id),
  })
    .populate("author")
    .sort({
      postedTime: -1,
    })
    .catch((err) => res.status(404).send(err)); // newest to oldest

  if (!reviewsList) {
    res.status(500).json({ success: false });
  }
  var sizeArr = reviewsList.length;

  res.status(200).send(reviewsList[sizeArr - 1]);
});

//Get Review By Review Id
router.get("/getReviewData/:id", async (req, res) => {
  if (!mongoose.isValidObjectId(req.params.id)) {
    return res.status(400).json({
      success: false,
      message: "Exeption occured.\nPlease try again later",
    });
  }
  Review.find({ _id: mongoose.Types.ObjectId(req.params.id) })
    .populate("author")
    .then((review) => {
      if (!review) return res.status(404).send("Review not found");
      res.status(200).send(review);
    })
    .catch((err) => res.status(404).send(err));
});

//Get Reviews By Post Id
router.get("/getPostReviews/:id", async (req, res) => {
  if (!mongoose.isValidObjectId(req.params.id)) {
    return res.status(400).json({
      success: false,
      message: "Exeption occured.\nPlease try again later",
    });
  }

  Review.find({ post: mongoose.Types.ObjectId(req.params.id) })
    .populate("author")
    .then(function (reviews) {
      res.send(reviews); // Success
    })
    .catch(function (error) {
      res.status(500).send(error); // Failure
    });
});

//Update review
router.put("/:id", verifyToken, async (req, res) => {
  if (!mongoose.isValidObjectId(req.params.id)) {
    return res.status(400).send("Review not exist");
  }
  const review = await Review.findByIdAndUpdate(
    mongoose.Types.ObjectId(req.params.id),
    {
      body: req.body.body,
    },
    { new: true }
  )
    .then((review) => res.status(200).send(review))
    .catch((err) => res.status(400).send(err));
});

//Delete review
router.delete("/:id", verifyToken, async (req, res) => {
  if (!mongoose.isValidObjectId(req.params.id)) {
    return res.status(400).json({
      success: false,
      message: "Exeption occured.\nPlease try again later",
    });
  }

  Review.deleteOne({
    _id: mongoose.Types.ObjectId(req.params.id),
  })
    .then(function () {
      return res
        .status(200)
        .json({ success: true, message: "The Review has been deleted" });
    })
    .catch(function (error) {
      return res
        .status(404)
        .json({ success: false, message: "Cannot find the Review" });
    });
});

//Get the count of the reviews
router.get("/get/count", async (req, res) => {
  Review.countDocuments((count) => count)
    .then((countReviews) =>
      res.status(200).send({ countReviews: countReviews })
    )
    .catch((err) => res.status(400).send(err));
});

//Search by 3 params
router.get("/search3params", async (req, res) => {
  if (!mongoose.isValidObjectId(req.params.id)) {
    return res.status(400).json({
      success: false,
      message: "Exeption occured.\nPlease try again later",
    });
  }
  const conditions = {};
  conditions.post = mongoose.Types.ObjectId(req.query.postID);
  if (req.query.bodyReview) {
    // if title isnt empty add it to conditions for search
    conditions.body = { $regex: req.query.bodyReview, $options: "i" };
  }
  if (req.query.userName) {
    // if tags isnt empty add it to conditions for search
    const author = await User.findOne({
      name: { $regex: req.query.userName, $options: "i" },
    });
    if (author) {
      const authorID = mongoose.Types.ObjectId(author._id);
      conditions.author = authorID;
    }
  }
  if (req.query.fromDate) {
    // if from date isnt empty add it to conditions for search
    conditions.published = { $gte: req.query.fromDate };
  }
  if (req.query.toDate) {
    // if to date isnt empty add it to conditions for search
    conditions.published = {
      ...conditions.published,
      $lte: req.query.toDate,
    };
  }
  Review.find(conditions)
    .populate("author")
    .sort({ published: -1 }) // newest to oldest
    .then((reviews) => res.status(200).send(reviews))
    .catch((err) => res.status(404).send("Error: " + err));
});
module.exports = router;
