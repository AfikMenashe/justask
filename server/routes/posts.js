const express = require("express");
const router = express.Router();
const { Post } = require("../models/Post");
const { User } = require("../models/user");
const checkIfTagExist = require("../helper/checkIfTagExist");
const mongoose = require("mongoose");
const { Tag } = require("../models/tag");
const multer = require("multer");
const verifyToken = require("../helper/jwt");
const MongoClient = require("mongodb").MongoClient;

//Mongo Local DataBase
const url = "mongodb://127.0.0.1:27017";
const dbName = "JustAskLocalDB";
let localMongoDb;

//Map Reduce Function -> Returning an object of "_id"(of post) and "value"(numbers of reviews on specific post)
router.get("/mapReduceFunc/:id", async (req, res) => {
  const PostID = req.params.id;
  MongoClient.connect(
    url,
    { useNewUrlParser: true, useUnifiedTopology: true },
    async (err, client) => {
      if (err)
        return console.log("Error with Connecting to Local MongoDB : \n" + err);

      // Storing a reference to the database so you can use it later
      localMongoDb = client.db(dbName);
      console.log(`Connected LocalMongoDB: ${url}`);
      console.log(`Database JustAskLocalDB: ${dbName}`);

      var mapFunction1 = function () {
        emit(this._id, this.reviews);
      };

      var reduceFunction1 = function (keyId, values) {
        var ArrReviews = Array.sum(values);
        return ArrReviews.length;
      };

      var MapReduceData = await localMongoDb
        .collection("Post")
        .mapReduce(mapFunction1, reduceFunction1, {
          out: "MapReduce",
        });

      if (MapReduceData != null) {
        localMongoDb
          .collection("MapReduce")
          .find({ _id: mongoose.Types.ObjectId(PostID) })
          .toArray((err, results) => {
            if (err) throw err;

            res.send(results[0]);
          });
      }
    }
  );
});

/////////////////////////////////

//TODO: change line 20 -> change the name of the file to name with the user ID

// <----------- multer ----------->

const multerStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "uploads/");
  },
  // To create a filename, we use the user ID and the current timestamp
  filename: (req, file, cb) => {
    const ext = file.mimetype.split("/")[1];
    // cb(null, `user-${req.user.id}-${Date.now()}.${ext}`);
    cb(null, `user-123123-${Date.now()}.${ext}`);
  },
});
// Check if the file is actually an image
const multerFilter = (req, file, cb) => {
  if (file.mimetype.startsWith("image")) {
    cb(null, true);
  } else {
    cb(new AppError("Not an image! Please upload an image.", 400), false);
  }
};

const uploadOptions = multer({
  storage: multerStorage,
  fileFilter: multerFilter,
});

// <----------- multer -----------/>

//Get all Posts
router.get("/", async (req, res) => {
  Post.find()
    .populate("author")
    .sort({ postedTime: -1 }) // newest to oldest
    .then((posts) => res.send(posts))
    .catch((err) => res.status(404).send(err));
});

router.get("/ofSpecUser/:id", async (req, res) => {
  var userID = req.params.id;
  // var userID = "606dcbf77dd2707038249fb3";

  Post.aggregate(
    [{ $group: { _id: "$author", posts: { $push: "$$ROOT" } } }],
    async function (err, result) {
      const PostsOfUser = result.find((x) => {
        if (x._id == userID) {
          return x;
        }
      });
      res.send(PostsOfUser);
    }
  );
});

//Search by 3 params
router.get("/search3params", async (req, res) => {
  const conditions = {};

  if (req.query.titlePost) {
    // if title isnt empty add it to conditions for search
    conditions.title = { $regex: req.query.titlePost, $options: "i" };
  }
  if (req.query.tags) {
    // if tags isnt empty add it to conditions for search
    conditions.tags = { $in: req.query.tags };
  }
  if (req.query.fromDate) {
    // if from date isnt empty add it to conditions for search
    conditions.postedTime = { $gte: req.query.fromDate };
  }
  if (req.query.toDate) {
    // if to date isnt empty add it to conditions for search
    conditions.postedTime = {
      ...conditions.postedTime,
      $lte: req.query.toDate,
    };
  }
  Post.find(conditions)
    .populate("tags", "author")
    .sort({ postedTime: -1 }) // newest to oldest
    .then((posts) => res.send(posts))
    .catch((err) => res.status(404).error("Error: " + err));
});

//Get Post by id
router.get("/getpost", async (req, res) => {
  if (!mongoose.isValidObjectId(req.query.postid)) {
    return res.status(400).send("User id invalid");
  }

  await Post.findOne({ _id: mongoose.Types.ObjectId(req.query.postid) })
    .populate("author reviews tags")
    .then((post) => {
      if (!post) return res.status(404).send("Post not found");
      res.send(post);
    })
    .catch((err) => res.status(404).send("Error getting the post: " + err));
});

//Create Post
router.post(
  "/addPost",
  verifyToken,
  uploadOptions.array("images", 10),
  async (req, res) => {
    // validate all required data is passed
    if (!req.body.title || !req.body.description) {
      return res
        .status(400)
        .send("The Post can not be created - missing parameters");
    }

    //create the Post
    const new_post = new Post({
      title: req.body.title,
      description: req.body.description,
      postedTime: Date.now(),
      author: mongoose.Types.ObjectId(req.body.author),
      images: [],
      tags: [],
    });

    //Check if any files uploaded -> add the url to
    if (req.files) {
      const files = req.files;
      let imagesPaths = [];
      const basePath = `${req.protocol}://${req.get("host")}/uploads/`;
      files.map((file) => {
        imagesPaths.push(`${basePath}${file.filename}`);
      });
      new_post.images = imagesPaths;
    }

    //Check if the tag is exist in DB and return array of tags id
    if (req.body.tags) {
      var allTags = await Promise.all(
        req.body.tags.split(",").map(async (tagName) => {
          let existTag = await checkIfTagExist(tagName);
          if (!existTag) {
            // if tag doesn't exist create it'
            const createdTag = new Tag({
              type: tagName,
              posts: [new_post._id],
            });
            await createdTag.save();
            return createdTag._id;
          }
          // if tag is exist add the Post it to posts array
          existTag.posts = [...existTag.posts, new_post._id];
          await existTag.save();
          return existTag._id;
        })
      );
      new_post.tags = allTags;
    }

    //Updating the post-history of the specific user
    User.updateOne(
      { _id: req.body.author },
      { $push: { post_history: new_post._id } },
      function (err, result) {
        if (err) {
          res.status(400).send(err);
        }
      }
    );

    //save the Post to db
    new_post
      .save()
      .then((item) => {
        res.status(200).send(item);
      })
      .catch((err) => {
        console.error("creating new post: " + err);
        res.status(400).send(err);
      });
  }
);

//Update exist Post
router.put("/:id", verifyToken, async (req, res) => {
  console.log("Got there");
  if (!mongoose.isValidObjectId(req.params.id)) {
    return res.status(400).send("Post not exist");
  }
  const post = await Post.findByIdAndUpdate(
    req.params.id,
    {
      title: req.body.title,
      description: req.body.description,
    },
    { new: true }
  )
    .then((post) => res.send(post))
    .catch((err) => res.status(400).send(err));
});

//Delete Post
router.delete("/:id", verifyToken, async (req, res) => {
  if (!mongoose.isValidObjectId(req.params.id)) {
    return res.status(400).json({
      success: false,
      message: "Exeption occured.\nPlease try again later",
    });
  }

  Post.deleteOne({
    _id: mongoose.Types.ObjectId(req.params.id),
  })
    .then(function () {
      return res
        .status(200)
        .json({ success: true, message: "The Post has been deleted" });
    })
    .catch(function (error) {
      return res
      .status(404)
      .json({ success: false, message: "Cannot find the Post" });
    });
});

//Get the count of the Post
router.get("/get/count", verifyToken, async (req, res) => {
  Post.countDocuments((count) => count)
    .then((countPost) => res.status(200).send({ countPost: countPost }))
    .catch((err) => res.status(400).send(err));
});

router.put(
  "/gallery-images/:id",
  uploadOptions.array("images", 10),
  async (req, res) => {
    if (!mongoose.isValidObjectId(req.params.id)) {
      return res.status(400).send("Invalid Product Id");
    }
    const files = req.files;
    let imagesPaths = [];
    const basePath = `${req.protocol}://${req.get("host")}/public/uploads/`;

    if (files) {
      files.map((file) => {
        imagesPaths.push(`${basePath}${file.filename}`);
      });
    }

    const product = await Product.findByIdAndUpdate(
      req.params.id,
      {
        images: imagesPaths,
      },
      { new: true }
    );

    if (!product) return res.status(500).send("the gallery cannot be updated!");

    res.send(product);
  }
);

module.exports = router;
