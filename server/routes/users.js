const express = require('express');
const router = express.Router();
const { User } = require('../models/user');
const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const verifyToken = require('../helper/jwt');

//Get all users
router.get('/', async (req, res) => {
  User.find()
    .populate('reviews')
    .then((users) => res.send(users))
    .catch((err) => {
      res.status(400).send('Error: ' + err);
    });
});

//Get user by id
router.get('/:id', async (req, res) => {
  if (!mongoose.isValidObjectId(req.params.id)) {
    return res.status(400).json({
      success: false,
      message: 'Exeption occured.\nPlease try again later',
    });
  }
  User.findOne({ _id: req.params.id })
    .then((user) => {
      if (!user) return res.status(404).send('User not found');
      res.send(user);
    })
    .catch((err) => {
      res.status(400).send('Error: ' + err);
    });
});

//Get the count of the users
router.get('/get/topusers', async (req, res) => {
  console.log('hey');
  User.aggregate([
    {
      $project: {
        type: 1,
        name: '$name',
        reviews_length: { $size: '$created_reviews' },
      },
    },
    { $sort: { reviews_length: -1 } },
  ])
    .limit(5)
    .then((users) => res.status(200).send(users))
    .catch((err) => {
      res.status(500).send('Error: ' + err);
    });
  // res.status(200).send('hey');
});

//Create user
router.post('/addUser', (req, res) => {
  //validate all required data is passed
  if (!req.body.user.name || !req.body.user.password || !req.body.user.email) {
    return res
      .status(400)
      .send('The User can not be created - missing parameters');
  }

  //create the user
  const user = new User({
    name: req.body.user.name,
    passwordHash: bcrypt.hashSync(req.body.user.password, 10),
    singUpTime: Date.now(),
    email: req.body.user.email,
    image: req.body.user.image,
    isAdmin: req.body.user.isAdmin,
  });

  //save the user to db
  user
    .save()
    .then((user) => {
      res.send(user);
    })
    .catch((err) => {
      res.status(400).send(err);
    });
});

//Login
router.post('/login', async (req, res) => {
  if (!req.body.email) return res.status(400).send('Missing email parameter');

  const user = await User.findOne({ email: req.body.email }).catch((err) => {
    res.status(404).send('Error: ' + err);
  });
  const secret = process.env.secret;

  //check if the mail exist in db
  if (!user) return res.status(404).send('The user is not found');

  //check if the password is correct
  if (user && bcrypt.compareSync(req.body.password, user.passwordHash)) {
    const token = jwt.sign(
      {
        userId: user.id,
      },
      secret,
      {
        expiresIn: '1d', //expires time - 1h, 1d , 1w ,1m
      }
    );
    res.status(200).send({ user: user, token: token });
  } else {
    res.status(401).send('Password is wrong');
  }
});

//Update user
router.put('/:id', verifyToken, async (req, res) => {
  if (!mongoose.isValidObjectId(req.params.id)) {
    return res.status(400).send('User not exist');
  }
  const user = await User.findByIdAndUpdate(
    req.params.id,
    {
      name: req.body.fullName,
      email: req.body.email,
    },
    { new: true }
  )
    .then((user) => res.send(user))
    .catch((err) => {
      res.status(400).send('Error: ' + err);
    });
});

//Delete user
router.delete('/:id', verifyToken, async (req, res) => {
  if (!mongoose.isValidObjectId(req.params.id)) {
    return res.status(400).json({
      success: false,
      message: 'Exeption occured.\nPlease try again later',
    });
  }

  User.deleteOne({
    _id: mongoose.Types.ObjectId(req.params.id),
  })
    .then(function () {
      return res
        .status(200)
        .json({ success: true, message: "The User has been deleted" });
    })
    .catch(function (error) {
      return res
      .status(404)
      .json({ success: false, message: "Cannot find the User" });
    });
});

//Get the count of the users
router.get('/get/count', async (req, res) => {
  User.countDocuments((count) => count)
    .then((countUsers) => res.send({ countUsers: countUsers }))
    .catch((err) => {
      res.status(400).send('Error: ' + err);
    });
});

module.exports = router;
