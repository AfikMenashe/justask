const express = require("express");
const app = express();
const bodyparser = require("body-parser");
const morgan = require("morgan");
const mongoose = require("mongoose");
const router = express.Router();
const cors = require("cors");
const authJwt = require("./helper/jwt");
const errorHanler = require("./helper/error-handler");
const socketIo = require("socket.io");
const http = require("http");

//Enable CORS policy
app.use(cors());
app.options("*", cors());

//Access to .env file (use for save global variables - UNMODIFED)
require("dotenv/config");

//Middleware
app.use(bodyparser.json());
// app.use(morgan("tiny"));
// app.use(authJwt);
app.use("/uploads", express.static("uploads"));
// app.use("/public/uploads", express.static(__dirname + "/public/uploads"));
app.use(errorHanler);

//Routes
const Posts = require("./routes/posts");
const users = require("./routes/users");
const tags = require("./routes/tags");
const reviews = require("./routes/reviews");
const api = require("./routes/externalAPI");
const { randomInt } = require("crypto");

//All the Posts routes are relative to the first argument string
app.use("/posts", Posts);
app.use("/users", users);
app.use("/tags", tags);
app.use("/reviews", reviews);
app.use("/api", api);

//DataBase connection
mongoose
  .connect(process.env.CONNECTION_STRING, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    dbName: "JustAskDataBase",
  })
  .then(() => {
    console.log("DB Connection is ready...");
    console.log("Look for TODO in Project");
  })
  .catch((err) => {
    console.log(`@ Error occured: ${err}`);
  });

//socket io
const server = http.createServer(app);
const io = socketIo(server, {
  cors: {
    origins: ["http://localhost:4200", "http://localhost:3000"],
    methods: ["GET", "POST"],
    credentials: true,
  },
});

var count = 0;
var socktArray = [];
io.on("connection", (socket) => {
  if (
    socket.handshake.headers.origin === "http://localhost:3000" ||
    socket.handshake.headers.origin === "http://localhost:4200"
  ) {
    count++;
    socket.emit("count", count);
    socket.broadcast.emit("count", count);
    console.log(`Connected ${count}`);
    socktArray.push(socket);
    socket.on("disconnect", () => {
      count--;
      socket.broadcast.emit("count", count);
      // socket.broadcast.emit('disconnect',count);
      console.log(`disonnected ${count}`);
    });
  }
});

//Disconnect all connections
// router.post("/disconnectall", async (req, res) => {
//   socktArray.forEach((socket) => {
//     io.to(socket.id).emit("disconnect", 5);
//   });
//   // count = 0;
//   socktArray = [];
//   res.send("Disconnected all connections");
// });
// app.use("/", router);

//Start server
server.listen(8080, () => {
  console.log("Server is running on port 8080");
});
