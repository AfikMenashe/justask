const mongoose = require('mongoose');

var tagSchema = mongoose.Schema({
  type: {
    type: String,
    required: true,
  },
  posts: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Post',
      required: false,
    },
  ],
});

tagSchema.pre('deleteOne', async function () {
  const tag = this.getQuery();
  // Remove all the posts that reference the removed tag.
  mongoose
    .model('Post')
    .updateMany({}, { $pull: { tags: mongoose.Types.ObjectId(tag._id) } })
    .exec()
    .then(function () {
      console.log('Tag-Pre posts update sucssed'); // Success
    })
    .catch(function (error) {
      console.error('Tag-Pre posts update failed: ' + error); // Failure
    });
});
exports.Tag = mongoose.model('Tag', tagSchema);
