const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  passwordHash: {
    type: String,
    required: true,
  },
  singUpTime: {
    type: Date,
    required: true,
    default: Date.now,
  },
  image: {
    type: String,
    required: false,
    default:"https://romancebooks.co.il/wp-content/uploads/2019/06/default-user-image.png"
  },
  created_reviews: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Review',
      required: false,
    },
  ],
  post_history: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Post',
      required: false,
    },
  ],
  isAdmin: {
    type: Boolean,
    required: false,
  },
});

userSchema.set('toJSON', {
  virtuals: true,
});

userSchema.pre('deleteOne', async function () {
  const user = this.getQuery();
  // Remove all the users that reference the removed user.
  mongoose
    .model('Post')
    .deleteMany({ author: mongoose.Types.ObjectId(user._id) })
    .exec()
    .then(function () {
      console.log('User-Pre posts deleted sucssed'); // Success
    })
    .catch(function (error) {
      console.error('User-Pre posts deleted failed: ' + error); // Failure
    });

  // Remove all the reviews that reference the removed User.
  mongoose
    .model('Review')
    .deleteMany({ author: mongoose.Types.ObjectId(user._id) })
    .exec()
    .then(function () {
      console.log('User-Pre reviews deleted sucssed'); // Success
    })
    .catch(function (error) {
      console.error('User-Pre reviews deleted failed: ' + error); // Failure
    });
});

exports.User = mongoose.model('User', userSchema);
