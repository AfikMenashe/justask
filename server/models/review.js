const mongoose = require('mongoose');

const reviewSchema = mongoose.Schema({
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  published: {
    type: Date,
    required: true,
    default: Date.now,
  },
  body: {
    type: String,
    required: true,
  },
  post: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Post',
  },
});

reviewSchema.pre('deleteOne', async function () {
  const review = this.getQuery();
  // Remove all the posts that reference the removed review.
  await mongoose
    .model('Post')
    .updateMany({}, { $pull: { reviews: mongoose.Types.ObjectId(review._id) } })
    .exec()
    .then(function () {
      console.log('Review-Pre posts update sucssed'); // Success
    })
    .catch(function (error) {
      console.error('Review-Pre posts update failed: ' + error); // Failure
    });
});
exports.Review = mongoose.model('Review', reviewSchema);
