const mongoose = require('mongoose');

const postSchema = mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  postedTime: {
    type: Date,
    required: true,
    default: Date.now,
  },
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  images: [
    {
      type: String,
      required: false,
    },
  ],
  tags: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Tag',
      required: false,
    },
  ],
  reviews: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Review',
      required: false,
    },
  ],
});

postSchema.pre('deleteOne', async function () {
  const post = this.getQuery();
  // Remove all the tags and reviews that reference the removed Post.
  mongoose
    .model('Tag')
    .updateMany({}, { $pull: { posts: mongoose.Types.ObjectId(post._id) } })
    .exec()
    .then(function () {
      console.log('Post-Pre tags update sucssed'); // Success
    })
    .catch(function (error) {
      console.error('Post-Pre tags update failed: ' + error); // Failure
    });
  // Remove all the reviews that reference the removed Post.
  mongoose
    .model('Review')
    .deleteMany({ post: mongoose.Types.ObjectId(post._id) })
    .exec()
    .then(function () {
      console.log('Post-Pre reviews deleted sucssed'); // Success
    })
    .catch(function (error) {
      console.error('Post-Pre reviews deleted failed: ' + error); // Failure
    });

  // TODO: If the database is chagnged - > changed the 'created_reviews'
  mongoose
    .model('User')
    .updateMany(
      { _id: mongoose.Types.ObjectId(post.author) },
      { $pull: { created_reviews: mongoose.Types.ObjectId(post._id) } }
    )
    .exec()
    .then(function () {
      console.log('Post-Pre user update sucssed'); // Success
    })
    .catch(function (error) {
      console.error('Post-Pre user update failed: ' + error); // Failure
    });
});
exports.Post = mongoose.model('Post', postSchema);
