import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListItemComponent } from './components/list-item/list-item.component';
import { ListComponent } from './list/list.component';
// import {HttpClient} from '@angular/common/http'
import { HttpClientModule } from '@angular/common/http';
import { ListCommentComponent } from './components/list-comment/list-comment.component';
import { ListTagComponent } from './components/list-tag/list-tag.component';
import { ListUserComponent } from './components/list-user/list-user.component';
import { UserCounterComponent } from './components/user-counter/user-counter.component';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
const config: SocketIoConfig = { url: 'http://localhost:8080' };
import { PrecentTagComponent } from './components/precent-tag/precent-tag.component';
import { RecentCommentComponent } from './components/recent-comment/recent-comment.component';
// D3-Graph
import { TagGraphComponent } from './components/tag-graph/tag-graph.component';
import { D3GraphComponent } from './components/d3-graph/d3-graph.component';
import { UserGraphComponent } from './components/user-graph/user-graph.component';
// Login and alert for problems:401/404....
import { LoginComponent } from './components/login/login.component';
import { NgbdAlertCloseable } from './components/alert/alert-closeable';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
// search module
import { Ng2SearchPipeModule } from 'ng2-search-filter';
@NgModule({
  declarations: [
    AppComponent,
    ListItemComponent,
    ListComponent,
    ListCommentComponent,
    ListTagComponent,
    ListUserComponent,
    UserCounterComponent,
    PrecentTagComponent,
    RecentCommentComponent,
    TagGraphComponent,
    D3GraphComponent,
    UserGraphComponent,
    LoginComponent,
    NgbdAlertCloseable,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    SocketIoModule.forRoot(config),
    NgbModule,
    Ng2SearchPipeModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
