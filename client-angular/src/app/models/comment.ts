import {Post} from './post';
import {User} from './user';

export interface Comment{
    _id:string,
    body:string,
    published:Date,
    author:User,
    post:Post
}