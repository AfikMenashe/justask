import {Post} from './post';
import {Comment} from './comment';

export interface User{
    _id:string,
    name:string,
    singUpTime:Date,
    email:string,
    passwordHash:string,
    image:string,
    created_reviews:[Comment],
    post_history:[Post],
    isAdmin:Boolean
}