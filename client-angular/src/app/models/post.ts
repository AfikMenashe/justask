import {Comment} from './comment';
import {User} from './user';
import {Tag} from './tag';

export interface Post{
    _id:string,
    title:string,
    description:string,
    postedTime:Date,
    author:User,
    images:[string],
    tags:[Tag],
    reviews:[Comment]
}