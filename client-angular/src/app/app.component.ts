import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { PostsService } from './services/posts.service';
import { Post } from './models/post';
import { Tag } from './models/tag';
import { Comment } from './models/comment';
import { User } from './models/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'client-angular';
  searchText:any;
  isAuthorized: boolean = false;
  myPosts: Post[] = [];
  myTags: Tag[] = [];
  myComments: Comment[] = [];
  myUser: User[] = [];
  orderedTags: any = [];
  orderedUsers: User[] = [];
  orderedComments: any[] = [];
  public name: string = 'yariv';
  constructor(private postService: PostsService) {
    this.verifyAdmin();
  }

  ngOnInit(): void {
    this.load();
    this.verifyAdmin();
  }
  public boundedPrintValue = (param: any) => {
    console.log(param, this.name);
  };
  verifyAdmin(): void {
    let token = sessionStorage.getItem('token');

    if (token) this.isAuthorized = true;
  }

  onDelete(id: string) {
    this.postService.deletePost(id).subscribe(() => {
      console.log('deleted...');
    });
  }

  load() {
    this.postService.getUsers().subscribe((data) => {
      this.myUser = data;
      this.calculateRecentUsers();
    });

    this.postService.getPosts().subscribe((data) => {
      this.myPosts = data;
    });

    this.postService.getTags().subscribe((data) => {
      this.myTags = data;
      this.calculateTags();
    });

    this.postService.getReviews().subscribe((data) => {
      this.myComments = data;
      this.calculateRecentComments();
    });
  }

  //Helper functions
  currentDiv = '';

  public myFunction(div: string) {
    var x = document.getElementById(div);

    if (x !== null) {
      if (x.style.display === 'none') {
        x.style.display = 'block';
      } else {
        x.style.display = 'none';
      }
      this.currentDiv = div;
    }
    this.hideAllOtherDivs(div);
  }

  public hideAllOtherDivs(div: string) {
    var x = document.getElementById('postsDiv');
    var y = document.getElementById('commentsDiv');
    var z = document.getElementById('usersDiv');
    var w = document.getElementById('tagsDiv');
    if (x !== null && x.id !== div) x.style.display = 'none';
    if (y !== null && y.id !== div) y.style.display = 'none';
    if (z !== null && z.id !== div) z.style.display = 'none';
    if (w !== null && w.id !== div) w.style.display = 'none';
  }

  public calculateTags() {
    this.myTags.forEach((tag) => {
      this.orderedTags.push({ type: tag.type, count: tag.posts.length });
    });
    this.orderedTags.sort((a: any, b: any) => b.count - a.count);
  }

  public calculateRecentUsers() {
    this.myUser.forEach((user) => {
      this.orderedUsers.push(user);
    });
    this.orderedUsers.sort(
      (a: User, b: User) =>
        new Date(b.singUpTime).getTime() - new Date(a.singUpTime).getTime()
    );
  }

  public calculateRecentComments() {
    this.myComments.forEach((comment) => {
      let userFound = this.findUserByID(comment.author);
      let nameFound = userFound ? userFound.name : '';
      let imageFound = userFound ? userFound.image : '';
      this.orderedComments.push({
        comment: comment,
        authorName: nameFound,
        imageUrl: imageFound,
      });
    });
    this.orderedComments.sort(
      (a: any, b: any) =>
        new Date(b.comment.published).getTime() -
        new Date(a.comment.published).getTime()
    );
  }

  public findUserByID(userRev: User) {
    return this.myUser.find((user) => user._id === userRev.toString());
  }
}
