import { Injectable } from '@angular/core';
import {Socket} from 'ngx-socket-io';

@Injectable({
  providedIn: 'root'
})
export class RealtimeService {

  currentCount = this.socket.fromEvent<Number>('count');
  posts = this.socket.fromEvent<Number>('disconnect');


  constructor(private socket: Socket) {}
}
