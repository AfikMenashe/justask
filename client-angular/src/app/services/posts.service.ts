import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Post } from '../models/post';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Tag } from '../models/tag';
import { Comment } from '../models/comment';
import { User } from '../models/user';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class PostsService {
  private postsUrl = environment.baseUrl;
  private token = sessionStorage.getItem('token');

  header = {
    headers: new HttpHeaders().set('Authorization', `Bearer ${this.token}`),
  };

  constructor(private http: HttpClient) {}

  errorHandler(error: HttpErrorResponse) {
    return throwError(error.message || 'server error.');
  }

  getPosts(): Observable<Post[]> {
    return this.http.get<Post[]>(this.postsUrl + '/posts');
  }

  deletePost(id: string): Observable<Post> {
    const url = `${this.postsUrl}/posts/${id}`;
    return this.http.delete<Post>(url, this.header);
  }

  getTags(): Observable<Tag[]> {
    return this.http.get<Tag[]>(this.postsUrl + '/tags');
  }
  getTopTags(): Observable<Tag[]> {
    return this.http.get<Tag[]>(this.postsUrl + '/tags/top5tags');
  }

  deleteTag(id: string): Observable<Tag> {
    const url = `${this.postsUrl}/tags/${id}`;
    return this.http.delete<Tag>(url, this.header);
  }

  getReviews(): Observable<Comment[]> {
    return this.http.get<Comment[]>(this.postsUrl + '/reviews');
  }

  deleteReview(id: string): Observable<Comment> {
    const url = `${this.postsUrl}/reviews/${id}`;
    return this.http.delete<Comment>(url, this.header);
  }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.postsUrl + '/users');
  }

  getTopUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.postsUrl + '/users/get/topusers');
  }

  deleteUser(id: string): Observable<User> {
    const url = `${this.postsUrl}/users/${id}`;
    return this.http.delete<User>(url, this.header);
  }

  login(
    email: string,
    password: string
  ): Observable<{ user: any; token: any }> {
    const url = `${this.postsUrl}/users/login`;
    const body = {
      email: email,
      password: password,
    };
    return this.http
      .post<{ user: any; token: any }>(url, body, this.header)
      .pipe(catchError(this.errorHandler));
  }
}
