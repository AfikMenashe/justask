import { Component, OnInit } from '@angular/core';
import {PostsService} from '../services/posts.service'

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  constructor(private postsService: PostsService ) {}

  ngOnInit(): void {
  }

  onDeletePost(id:string): void{
    this.postsService.deletePost(id).subscribe(()=>{
      console.log("deleted post with id: "+id);
    })
    
  }

}
