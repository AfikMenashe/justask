import {
  Component,
  OnInit,
  Input,
  ElementRef,
  ViewContainerRef,
} from '@angular/core';
import { AppComponent } from '../../app.component';
import { PostsService } from '../../services/posts.service';
import { Post } from '../../models/post';

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.css'],
})
export class ListItemComponent implements OnInit {
  @Input()
  post: Post;
  _parent: AppComponent;
  @Input() index: string;

  constructor(
    private postsService: PostsService,
    private elRef: ElementRef,
    private viewContainerRef: ViewContainerRef,
  ) {
    const _injector = this.viewContainerRef.parentInjector;
    this._parent = _injector.get<AppComponent>(AppComponent);
  }

  ngOnInit(): void {}

  onDeletePost(): void {
    this.postsService.deletePost(this.post._id).subscribe(() => {
      this.elRef.nativeElement.parentElement.style.display = 'none';
    });
  }
}
