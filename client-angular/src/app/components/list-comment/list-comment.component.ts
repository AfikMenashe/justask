import {
  Component,
  OnInit,
  Input,
  ElementRef,
  ViewContainerRef,
} from '@angular/core';
import { AppComponent } from '../../app.component';
import { PostsService } from '../../services/posts.service';
import { Comment } from '../../models/comment';

@Component({
  selector: 'app-list-comment',
  templateUrl: './list-comment.component.html',
  styleUrls: ['./list-comment.component.css']
})
export class ListCommentComponent implements OnInit {

  @Input()
  comment: Comment;
  _parent: AppComponent;
  @Input() index: string;

  constructor(
    private postsService: PostsService,
    private elRef: ElementRef,
    private viewContainerRef: ViewContainerRef,
  ) {
    const _injector = this.viewContainerRef.parentInjector;
    this._parent = _injector.get<AppComponent>(AppComponent);
  }

  ngOnInit(): void {}

  onDeletePost(): void {
    this.postsService.deleteReview(this.comment._id).subscribe((deleted) => {
      // console.log(this._parent.myPosts.length);
      // delete this._parent.myPosts[+this.index];
      // console.log('deleted');
      // console.log(this._parent.myPosts.length);
      this.elRef.nativeElement.parentElement.style.display = 'none';
    });
  }
}
