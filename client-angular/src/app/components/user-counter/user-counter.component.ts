import { Component } from '@angular/core';
import { RealtimeService } from '../../services/realtime.service';

@Component({
  selector: 'app-user-counter',
  templateUrl: './user-counter.component.html',
  styleUrls: ['./user-counter.component.css'],
})
export class UserCounterComponent {
  counter: Number = 0;

  constructor(private service: RealtimeService) {
    service.currentCount.subscribe((counter) => {
      console.log(counter);
      this.counter = counter;
    });

    // service.disconnect.subscribe((counter) => {
    //   console.log("here");
    //   //  document.open("http://localhost:4200", '_self').close();
    //   document.close();
    // });
  }
}
