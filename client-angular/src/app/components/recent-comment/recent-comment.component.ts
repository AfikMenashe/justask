import { Component, OnInit,Input } from '@angular/core';
import {User} from '../../models/user'
import {Comment} from '../../models/comment'

@Component({
  selector: 'app-recent-comment',
  templateUrl: './recent-comment.component.html',
  styleUrls: ['./recent-comment.component.css']
})
export class RecentCommentComponent implements OnInit {

  @Input()
  comment: Comment ;

  @Input()
  authorName: string ;

  @Input()
  authorImage: string ;

  constructor() { }

  ngOnInit(): void {
  }


}
