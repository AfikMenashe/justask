import {
  Component,
  OnInit,
  Input,
  ElementRef,
  ViewContainerRef,
} from '@angular/core';
import { AppComponent } from '../../app.component';
import { PostsService } from '../../services/posts.service';
import { User } from '../../models/user';


@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.css']
})
export class ListUserComponent implements OnInit {

  @Input()
  user: User;
  _parent: AppComponent;
  @Input() index: string;

  constructor(
    private postsService: PostsService,
    private elRef: ElementRef,
    private viewContainerRef: ViewContainerRef,
  ) {
    const _injector = this.viewContainerRef.parentInjector;
    this._parent = _injector.get<AppComponent>(AppComponent);
  }

  ngOnInit(): void {}

  onDeletePost(): void {
    this.postsService.deleteUser(this.user._id).subscribe((deleted) => {
      // console.log(this._parent.myPosts.length);
      // delete this._parent.myPosts[+this.index];
      // console.log('deleted');
      // console.log(this._parent.myPosts.length);
      this.elRef.nativeElement.parentElement.style.display = 'none';
    });
  }
}
