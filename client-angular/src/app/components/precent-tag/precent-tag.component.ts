import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-precent-tag',
  templateUrl: './precent-tag.component.html',
  styleUrls: ['./precent-tag.component.css']
})
export class PrecentTagComponent implements OnInit {

  @Input()
  item: any;

  constructor() { }

  ngOnInit(): void {
  }

}
