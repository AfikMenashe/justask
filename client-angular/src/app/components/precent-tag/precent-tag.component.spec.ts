import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrecentTagComponent } from './precent-tag.component';

describe('PrecentTagComponent', () => {
  let component: PrecentTagComponent;
  let fixture: ComponentFixture<PrecentTagComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrecentTagComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrecentTagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
